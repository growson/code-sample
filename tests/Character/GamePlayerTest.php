<?php

namespace Character;

use Game\Game;
use Game\GamePlayer;

class GamePlayerTest extends \PHPUnit_Framework_TestCase
{
    public function testPlayer()
    {
        $game = new Game();

        $player = new GamePlayer();
        $game->addPlayer($player);

        $this->assertEquals($game, $player->getGame());

        /** @var Character $character1 */
        $character1 = $this->getMockForAbstractClass('Character\Character', [$player]);

        $this->assertEquals($character1, $player->getCharacter());
    }
}
