<?php

namespace Character;

use Game\GamePlayer;

class CharacterTest extends \PHPUnit_Framework_TestCase
{
    public function testAddVote()
    {
        $player = new GamePlayer();

        /** @var Character $character1 */
        $character1 = $this->getMockForAbstractClass('Character\Character', [$player]);

        $character1->addVote();
        $this->assertEquals(1, $character1->getVotes());
    }

    public function testVoting()
    {
        $player1 = new GamePlayer();

        /** @var Character $character1 */
        $character1 = $this->getMockForAbstractClass('Character\Character', [$player1]);

        // add 1 vote to character
        $character1->addVote();
        $this->assertEquals(1, $character1->getVotes());

        // set character's votes to 5
        $character1->setVotes(5);
        $this->assertEquals(5, $character1->getVotes());

        // add 2 votes to character, should be 7 in result
        $character1->addVotes(2);
        $this->assertEquals(7, $character1->getVotes());


        $player2 = new GamePlayer();

        /** @var Character $character2 */
        $character2 = $this->getMockForAbstractClass('Character\Character', [$player2]);

        // another character votes for first character (gives him 1 vote)
        $character2->vote($character1);

        // character already has 5 votes, so it should become 6
        $this->assertEquals(8, $character1->getVotes());
    }
}
