<?php

namespace Character;

use Game\Game;
use Game\GamePlayer;

class GameTest extends \PHPUnit_Framework_TestCase
{
    public function testPlayers()
    {
        $game = new Game();

        $player1 = new GamePlayer();
        $game->addPlayer($player1);

        $player2 = new GamePlayer();
        $game->addPlayer($player2);

        $this->assertEquals(2, count($game->getPlayers()));

        $game->setPlayers([$player1]);

        $this->assertEquals(1, count($game->getPlayers()));
    }
}
