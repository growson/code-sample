<?php

namespace Character;

use Game\GamePlayer;

interface CharacterInterface
{
    /**
     * Adds 1 vote from this Character for another Character.
     *
     * @param CharacterInterface $character Character vote for
     * @return void
     */
    public function vote(CharacterInterface $character);

    /**
     * Sets votes count for Character.
     *
     * @param integer $votes
     * @return void
     */
    public function setVotes($votes);

    /**
     * Returns votes count for Character.
     *
     * @return integer
     */
    public function getVotes();

    /**
     * Adds 1 vote for this Character.
     *
     * @return void
     */
    public function addVote();

    /**
     * Adds specified votes for this Character.
     *
     * @param integer $votes
     */
    public function addVotes($votes);

    /**
     * Character team.
     *
     * @return string
     */
    public function getTeam();

    /**
     * Character name.
     *
     * @return string
     */
    public function getName();
    /**
     * GamePlayer who owns this Character.
     *
     * @return GamePlayer
     */
    public function getPlayer();
}
