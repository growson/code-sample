<?php

namespace Character;

use Game\GamePlayer;

abstract class Character implements CharacterInterface
{
    /**
     * Votes count.
     *
     * @var int
     */
    protected $votes = 0;

    /** @var  GamePlayer */
    protected $player;

    public function __construct(GamePlayer $player)
    {
        $player->setCharacter($this);
        $this->player = $player;
    }

    /**
     * {@inheritdoc}
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * {@inheritdoc}
     */
    public function vote(CharacterInterface $character)
    {
        $character->addVote();
    }

    /**
     * {@inheritdoc}
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;
    }

    /**
     * {@inheritdoc}
     */
    public function addVote()
    {
        $this->votes += 1;
    }

    /**
     * {@inheritdoc}
     */
    public function addVotes($votes)
    {
        $this->votes += $votes;
    }

    /**
     * {@inheritdoc}
     */
    public function getVotes()
    {
        return $this->votes;
    }
}
