<?php

namespace Game;

use Character\CharacterInterface;

class GamePlayer
{
    /**
     * Game
     *
     * @var Game
     */
    private $game;

    /**
     * Character
     *
     * @var CharacterInterface
     */
    private $character;

    /**
     * @param Game $game
     * @return GamePlayer
     */
    public function setGame(Game $game)
    {
        $this->game = $game;
        return $this;
    }

    /**
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @param CharacterInterface $character
     * @return GamePlayer
     */
    public function setCharacter(CharacterInterface $character)
    {
        $this->character = $character;

        return $this;
    }

    /**
     * @return CharacterInterface
     */
    public function getCharacter()
    {
        return $this->character;
    }
}
