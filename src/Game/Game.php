<?php

namespace Game;

class Game
{
    /**
     * Game Players
     *
     * @var array|GamePlayer[]
     */
    private $players = [];

    public function __construct()
    {
        $this->players = [];
    }

    /**
     * Adds new Player to game.
     *
     * @param GamePlayer $player
     * @return Game
     */
    public function addPlayer(GamePlayer $player)
    {
        $player->setGame($this);
        $this->players[] = $player;

        return $this;
    }

    /**
     * @return array|GamePlayer[]
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @param array|GamePlayer[] $players
     * @return Game
     */
    public function setPlayers($players)
    {
        $this->players = $players;

        return $this;
    }
}
